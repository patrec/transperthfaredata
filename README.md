# Transperth Historical Fares Dataset

**Compiled by Tristan W. Reed, Planning and Transport Research Centre (PATREC)**

This set of historical Transperth fares was compiled for the purposes of
statistical modelling undertaken by the Centre using the PTA's Annual Reports
alongside Wayback Machine snapshots of the Transperth website. As such, the
data itself is not owned by PATREC but rather by the PTA.

## Data Dictionary

The data itself takes the form of a tabular comma-separated value (CSV) file
which contains the following attributes:

* `EffectiveDate`: the date upon which the fare became effective. Note that the
  date the fare no longer became effective is not detailed; more discussion is
  included below.
* `FareClass`: the 'class' of fare which the record is describing, which is
  either one of `Standard`, `Concession` or `Student`.
* `FareType`: the 'type' of fare which the record is describing, which is
  either `Zone 1` through `Zone 9` (or `8` or `8/9` in early records),
  `2 Sections` for the fixed two-mile fare, `All Day` or `DayRider` for
  particular all day fares or the special `FamilyRider`, `FootyRider` or
  `MaxiRider` tickets. `Student` class fares will also have a `Student` value
  for the fare type.
* `PaymentType`: the type of payment required for this particular fare
  combination. This is either `Cash`, `MultiRider` (equivalent to another
  option, `MultiRider 10`), `MultiRider Plus` (the same to `MultiRider 40`) or
  `SmartRider (XX% Discount)`, where `XX` can be `10`, `15`, `20` or `25`. The
  different discounts are used in pairs, where the larger discount is for
  automatic reloading of the SmartRider card. The `15` became `10` at one point
  in time, as did the `25` to `20`.

## Data Quality

The data is described below in terms of each fare period and the observations
that took place when gathering each piece.

| Effective From | Effective To | Notes |
| -------------- | ------------ | ----- |
| 20/04/1998 | **01/07/2000** | [This is the oldest snapshot](https://web.archive.org/web/19990222125940/http://www.transperth.wa.gov.au/t_fares.html). No end date is known so it is assumed but there may have been changes in-between. |
| 01/07/2000 | 01/07/2001 | The 01/07/2000 fares were still in effect by 15/06/2001 – assumed no changes. |
| 01/07/2001 | 01/09/2001 | The 01/07/2001 fares were still in effect by 22/08/2001 – assumed no changes. The change at 01/09/2001 seems to be regarding fare rules rather than fare values. |
| 01/09/2001 | 01/07/2002 | The snapshot as of 04/06/2002 lists fares that are unchanged since 01/09/2001 as valid "to July 1, 2002". The snapshot as of 16/04/2002 shows that the fares valid since 01/09/2001 are still in effect. |
| 01/07/2002 | 07/07/2003 | Nomenclature was changed before 01/07/2002 but reflected the old pricing. Exact date of that occurrence is unsure, so have only applied forward. Still applies as per capture at 03/04/2003 and 04/06/2003 – assumed no changes. |
| 07/07/2003 | 01/07/2004 | [Snapshot as of 13/06/2004](https://web.archive.org/web/20040613021021/http://www.transperth.wa.gov.au/desktopdefault.aspx?tabid=26) shows these are still in effect – assumed no changes. |
| 01/07/2004 | 03/07/2005 | [Snapshot as of 16/06/2005](https://web.archive.org/web/20050616071314/http://www.transperth.wa.gov.au/desktopdefault.aspx?tabid=26) shows these are still in effect – assumed no changes. |
| 03/07/2005 | 01/07/2006 | Snapshot as of 25/06/2006 shows these are still in effect – assumed no changes. Same snapshot shows that fares increase as of 01/07/2006. Student MultiRider introduced during this time but as exact date is unsure, only added going forwards. |
| 01/07/2006 | 01/07/2007 | Notably, this is the first snapshot that contains both SmartRider and MultiRider data. [Snapshot at 09/06/2007](https://web.archive.org/web/20070609152721/http://www.transperth.wa.gov.au/TicketsandFares/GeneralFareInformation/tabid/111/Default.aspx) does not appear to have changed – assumed no changes. |
| 01/07/2007 | **01/07/2008** | Notably, this is the first snapshot that contains only SmartRider (and cash) data. It is also the first to contain nine zones (excluding a previous 8/9 combination). [The newest snapshot found is 29/02/2008](https://web.archive.org/web/20080229222916/http://www.transperth.wa.gov.au/TicketsandFares/GeneralFareInformation/tabid/111/Default.aspx) which lists fares effective July 2007. Further validation is desirable. |
| 01/07/2008 | 01/07/2009 | [Snapshot as at 13/06/2009](https://web.archive.org/web/20090613182104/https://www.transperth.wa.gov.au/TicketsandFares/GeneralFareInformation/tabid/111/Default.aspx) contains both "Fares Effective 1 July 2008" and "Fares Effective 1 July 2009". |
| 01/07/2009 | **01/07/2010** | [Snapshot on 18/02/2011](https://web.archive.org/web/20110218193100/http://www.transperth.wa.gov.au/TicketsandFares/GeneralFareInformation/tabid/111/Default.aspx) details "Fares Effective 1 July 2010" however no knowledge of in-between. |
| 01/07/2010 | **01/07/2011** | [Snapshot on 16/06/2011](https://web.archive.org/web/20110616035309/http://www.transperth.wa.gov.au/TicketsandFares.aspx) details current fares which match those from 2010 and new fares effective 1 July 2011. Needs further validation to confirm they indeed match. |
| 01/07/2011 | 01/07/2012 | [Snapshot on 09/06/2012](https://web.archive.org/web/20120609143427/http://www.transperth.wa.gov.au/TicketsandFares.aspx) details "fares effective Friday 1 July 2011" and "fares effective Sunday 1 July 2012". |
| 01/07/2012 | 01/07/2013 | [Snapshot on 06/06/2013](https://web.archive.org/web/20130606072022/http://www.transperth.wa.gov.au/TicketsandFares.aspx) details "fares effective Sunday 1 July 2012" – assumed no change. |
| 01/07/2013 | 01/07/2014 | [Snapshot on 10/06/2014](https://web.archive.org/web/20140610003843/http://www.transperth.wa.gov.au/Tickets-Fares/Fares) details "fares effective 1 July 2013" and "fares effective 1 July 2014". |
| 01/07/2014 | 01/11/2014 | [Snapshot on 30/10/2014](https://web.archive.org/web/20141030123151/https://www.transperth.wa.gov.au/tickets-fares/fares) details "fares effective 1 July 2014" and "fares effective 1 November 2014". Fares actually **decreased** after this period which is believed to be due to the repeal of the "carbon tax". |
| 01/11/2014 | 01/07/2015 | [Snapshot on 26/06/2015](https://web.archive.org/web/20150626071126/https://www.transperth.wa.gov.au/tickets-fares/fares) details "fares effective 1 November 2014" and "fares effective 1 July 2015". |
| 01/07/2015 | 01/07/2016 | [Snapshot on 14/06/2016](https://web.archive.org/web/20160614174953/https://www.transperth.wa.gov.au/tickets-fares/fares) details "fares effective 1 November 2015" and "fares effective 1 July 2016". |
| 01/07/2016 | 01/07/2017 | [Snapshot on 28/06/2017](https://web.archive.org/web/20170628053134/https://www.transperth.wa.gov.au/tickets-fares/fares) details "fares effective 1 November 2016" and "fares effective 1 July 2017". |
| 01/07/2017 | 01/07/2018 | [Snapshot on 01/06/2018](https://web.archive.org/web/20180601180339/https://www.transperth.wa.gov.au/tickets-fares/fares) details "fares effective 1 November 2017" and "fares effective 1 July 2018". |
| 01/07/2018 | 01/07/2019 | No snapshot between these dates, however the 2019 PTA Annual Report covering 01/07/2018 to 30/06/2019 only details the fares effective 01/07/2018 so it is assumed there are no other changes in this time period. |
| 01/07/2019 | 31/12/2021 | [Snapshot on 23/12/2021](https://web.archive.org/web/20211223072334/https://www.transperth.wa.gov.au/tickets-fares/fares). Noted that there was no fare increases in [2020](https://www.wa.gov.au/sites/default/files/2020-04/covid-19-state-government-support-for-households-fact-sheet.pdf) or [2021](https://www.mediastatements.wa.gov.au/Pages/McGowan/2021/05/Election-commitment-delivered-on-household-fees-and-charges.aspx) due to the COVID-19 pandemic. |
| 01/01/2022 | **20/01/2022** | [New 'two-zone' fare system introduced](https://web.archive.org/web/20220101190050/https://www.transperth.wa.gov.au/tickets-fares/fares). |

## Document Versioning Metadata

Last edited by Tristan Reed, 20 January 2022.
